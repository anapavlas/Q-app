
// Initialize Masonry gallery plugin

var $grid = $('.grid').imagesLoaded( function() {
  $grid.masonry({
  itemSelector: '.grid-item',
  columnWidth: '.grid-sizer',
  percentPosition: true,
  horizontalOrder: true
  });
});
